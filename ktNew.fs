let rec multiTwo list e =
open System
    match list with 
        | head :: tail when head = e -> head * 2 :: (multiTwo tail e)
        | head :: tail -> head :: (multiTwo tail e)
        | [] -> []

let rec powf x n =
    if n = 0. then 1.
        else x * powf x (n - 1.)
let p list x =
    let rec func i list x =
        match list with
            | [] -> 0.
            | head :: tail -> float(head) * (powf x i) + (func (i + 1.) tail x)
    func 0. (List.rev list) x

let rec sliv list1 list2 =
    match list1, list2 with
        | [ x ], [] -> x :: (sliv [] [])
        | [], [ x ] -> x :: (sliv [] [])
        | [ x ], [ y ] -> x :: (sliv [] [y])
        | [ x ], head :: tail -> x :: head :: (sliv [] tail)
        | head :: tail, [ x ] -> head :: x :: (sliv tail [])
        | head1 :: tail1, head2 :: tail2 -> head1 :: head2 :: (sliv tail1 tail2)
        | _, _ -> []

type BinTree =
    | Node of int*BinTree*BinTree
    | Empty

let myTree =
    Node (1,
        Node (0, Empty, Empty),
        Node (3,
            Node (2, Empty, Empty),
            Node (5, Empty, Empty)
        )
    )

// let countElem plus minus tree = 
//     let rec func tree = 
//         match tree with
//             | Node (data, left, right) -> 
//                 if data > 0 then
//                     countElem (plus + 1) minus left
                    

[<EntryPoint>]
let main argv =
    printfn "%A" (multiTwo [1;2;4;5;6;6;7] 6)
    printfn "%A" (p [1; 2; 1] 4.)
    printfn "%A" (sliv [1;2;3;4] [7;8;9;10;11])
    0