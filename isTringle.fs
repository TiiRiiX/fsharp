let isTriangle (a:float) (b:float) (c:float) =
    if (a + b > c) && (a + c > b) && (b + c > a) then
        if (((a*a + b*b) = c*c) || ((a*a + c*c) = b*b) || ((b*b + c*c) = a*a)) then
            (true, true)
        else
            (true, false)
    else
        (false, false)

[<EntryPoint>]
let main argv =
    //match isTriangle 3. 4. 5. with
    //match isTriangle 10. 4. 5. with
    match isTriangle 10. 6. 5. with
    | (a, b) -> if a && b then
                    printfn "Это прямоугольный треугольник"
                elif a then
                    printfn "Это обычный треугольник"
                else
                    printfn "Из этого нельзя построить треугольник"
    0