open System
open System.Diagnostics
open System.Web.UI.WebControls

let sumCubeInt x y = float(x*x*x + y*y*y)/2.0

let sumCubeFloat x (y:float) = (x*x*x + y*y*y)/2.0

let multiNumbers x:int = 
    (x / 1000) * (x / 100 % 10) * (x / 10 % 100 % 10) * (x % 10) 

let converter d =
    let y = d *25.4
    let m = int(y) / 1000
    let cm = (int(y) - m*1000) / 10
    let mm = y - float(m) * 1000.0 - float(cm) * 10.0
    (m, cm, mm)

let interval h1 m1 s1 h2 m2 s2 =
    let g = (h2 - h1) * 3600 + (m2 - m1) * 60 + s2 - s1
    (g / 3600, g / 60 % 60, g % 60)
        
let taxi p0 p1 p2 p3 =
    (p0 , p2 + p1 + p0, p3 + p2 + p1 + p0)

let energy k1 k2 k3 k = 
    match k with
        | x when x <= 500.0 -> k * k1
        | x when x <= 1000.0 -> 500.0 * k1 + (k - 500.0) * k2
        | _ -> 500.0 * k1 + 500.0 * k2 + (k - 1000.0) * k3
        
let camp t1 v1 t2 v2 t3 v3 =
    let s1 = t1 * v1
    let s2 = t2 * v2
    let s3 = t3 * v3
    let halfS = (s1 + s2 + s2)/2.0
    if halfS < s1 then
        (1, halfS / v1)
    elif halfS < s1 + s2 then
        (2, t1 + halfS / v2)
    else
        (3, t1 + t2 + halfS / v3)

let cows u v w k s t f =
    if (s * 100.0) / (k * u) < (t * 1000.0) / (k * v) then
        if (s * 100.0) / (k * u) < (f * 50.0) / (k * w) then
            ("Сено", (s * 100.0) / (k * u))
        else
            ("Комбикорм", (f * 50.0) / (k * w))
    else
        ("Силос", (t * 1000.0) / (k * v))

//Количество цифр в числе
let rec countNumbers a =
    if a = 0 then 0
        else countNumbers (a / 10) + 1

//НОД
let rec NOD a b =
    match b with
        |0 -> a
        |b -> NOD b (a % b)

//Вычисление 1 * 3 * 5 * 7 * ... * (2n - 1)
let rec rowMult n =
    match n with
        |1 -> 1
        |_ -> rowMult (n - 1) * (2 * n - 1)

let rowMultTails n =
    let rec funct n (acc:int64) =
        match n with
            |1 -> acc
            |_ -> funct (n - 1) (acc * int64(2 * n - 1))
    (funct n 1L)

//Вычисление факториала
let rec fact n = 
    if n = 0 then 1
        else n * fact(n - 1)

//Сумма факториалов 1! + 2! + 3! + ... + n!
let rec sumFact n =
    if n = 1 then 1
        else sumFact(n - 1) + fact (n)

//Сумма факториалов нечетных чисел между a и b
let rec sumOddNumbersAb a b =
    if a >= b then 
        if a % 2 = 0 then
            a + 1
        else 
            a
    else
        if b % 2 = 0 then
            sumOddNumbersAb a (b - 3) + fact(b - 1)
        else
            sumOddNumbersAb a (b - 2) + fact(b)

//Перевод числа n в p систему
let rec convertNumbers n p =
    if n < p then
        n.ToString()
    else 
        convertNumbers (n / p) p + (n % p).ToString()

//Игра со спичками
let rec game n = 
    if n = 1 then
        printfn "Вы проиграли"
    else
        printfn "Осталось %A спичек. Сколько спичек забрать?" n
        let take = int(Console.ReadLine())
        let taken = n - take
        match taken with
        |1 -> printfn "Победа!"
        |_ when take = taken -> printfn "Вы проиграли, спичек не осталось"
        |_ when taken < 0 -> printfn "Перебор, проиграли"
        |_ -> 
            let random = new Random()
            let compStep = random.Next(1,4)
            printfn "Компьютер забрал %A" compStep
            game (taken - compStep)

//Возведение целого числа x в степень n
let rec pown x n =
    if n = 0 then 1
        else x * pown x (n - 1)

//Перевод числа n из p системы в 10 систему
let rec convertBackNumber i n p =
    if n = 0 then
        0
    else 
        (convertBackNumber (i + 1) (n / 10) p) + (n % 10 * pown p i)

let useConvertNumberBack =
    convertBackNumber 0

//Проверяет простое ли число n (i - счетчик)
let rec isNumberSimple i n = 
    if float i < sqrt(float n) then 
        if n % i = 0 then
            false
        else
            isNumberSimple (i + 1) n 
    else
        true

let useIsNumberSimple =
    isNumberSimple 2

//Радиус вписанное окружности в n-угольник со стороной a
let radiusNPolygon n a =
    ((n - 2.0)/n * 180.0) * a

let rectRadius =
    radiusNPolygon 4.0

let tringleRadius =
    radiusNPolygon 3.0

//Взятие последнего элемента списка
let rec lastElementList list =
    match list with
        | [] -> -100000
        | [ element ] -> element
        | head :: tail -> lastElementList tail

//Удаление последнего элемента
let rec removeLastElement list = 
    match list with
        | [] -> []
        | [ element ] -> []
        | head :: tail -> head :: removeLastElement tail

//Удаление каждого второго элемента списка
let rec removeEventElements list =
    match list with
        | [x] -> [x]
        | [] -> []
        | head1 :: (head2 :: tail) -> head1 :: removeEventElements tail

let removeEvenElemetnsTails list =
    let rec funct list acc =
        match list with
            | [x] -> acc :: x
            | [] -> acc
            | head1 :: (head2 :: tail) -> (funct tail (acc :: head1))
    (funct list [])

//Сумма списков одинаковой длинны
let rec sumLists list1 list2 =
    match list1, list2 with
        | head1 :: tail1, head2 :: tail2 -> (head1 + head2) :: (sumLists tail1 tail2)
        | _ -> []

//list - список карт кортежей (масть, достоинство). Подсчет указанной масти
let rec findCard list color =
    match list with
        | (cardColor, cardValue) :: tail -> 
            if color = cardColor then
                1 + findCard tail color
            else
                findCard tail color
        | [] -> 0

//list - список фигур шахмат (фигура, горизонталь, вертикаль). Находится ли указанная фигура на указанной вертикали
let rec findCheess list figure findV =
    match list with 
        | (figureCheess, h, v) :: tail ->
            if figureCheess = figure && v = findV then
                true
            else
                findCheess tail figure findV
        | [] -> false

//Список списков в список элементов
let rec listOfListToList list =
    match list with
        | [] -> []
        | head :: tail -> head @ listOfListToList tail

//Подсчет количества элементов в списке
let rec countElem list elem =
    match list with
        | [] -> 0
        | head :: tail -> 
            if head = elem then
                1 + countElem tail elem
            else
                countElem tail elem

//list - список карт (масть, достоинство). Есть ли одинаковые карты на руках
let prepareFindSameCards list =
    let rec findSameCards list =
        match list with
            | [] -> false
            | head :: tail ->
                if countElem list head > 1 then
                    true
                else
                    findSameCards tail
    findSameCards (listOfListToList list)

[<EntryPoint>]
let main argv =
    //Практика на линейные алгоритмы
    printfn "%.3f" (sumCubeFloat 1.5 4.5)
    printfn "%.3f" (sumCubeInt 2 3)
    printfn "%i" (multiNumbers 1234)
    match converter 21.0 with
        | (m, cm, mm) -> printfn "%Am %Acm %Acm" m cm mm

    match interval 19 01 46 20 01 47 with
        | (h, m, s) -> printfn "%Aч %Aм %Aс" h m s

    match taxi 5 10 20 30 with
        | (t1, t2, t3) -> printfn "Первый: %A, Второй: %A, Третий: %A" t1 t2 t3

    //Практика на условные операторы
    match camp 3.0 5.0 4.0 6.0 4.0 4.0 with
        | (c, time) -> printfn "Участок: %A, Время: %A" c time
    match cows 2.0 3.0 2.0 5.0 3.0 1.0 7.0 with
        | (s, d) -> printfn "Быстрее кончится: %A за %A дней" s d
    printfn "Суммма долга: %A" (energy 10.0 5.0 3.0 700.0)

    //Практика на рекурсии
    printfn "Количество цифр: %A" (countNumbers 1001)
    printfn "НОД: %A" (NOD 7 15)
    printfn "Ряд: %A" (rowMult 4)
    printfn "Ряд с хвостовой: %A" (rowMultTails 15)
    printfn "Факториал: %A" (fact 4)
    printfn "Сумма факториалов: %A" (sumFact 3)
    printfn "Сумма факториалов нечетных : %A" (sumOddNumbersAb 1 9)
    printfn "Перевод чисел: %A" (convertNumbers 8 2)
    //game 23
    printfn "Перевод чисел обратно: %A" (convertBackNumber 0 12 3)
    printfn "Перевод чисел обратно (каррирование): %A" (useConvertNumberBack 12 3)
    printfn "Простое ли число: %A" (useIsNumberSimple 12) 
    printfn "Радиус окружности вписанной в окружность: %A" (rectRadius 3.0)
    //Практика на обработку списков и кортежей
    printfn "Последний элемент списка: %A" (lastElementList [1;2;3;4])
    printfn "Удаление последнего элемента списка: %A" (removeLastElement [1;2;3;4])
    printfn "Удаление каждого второго элемента: %A" (removeEventElements [1;2;3;4;5;6])
    printfn "Удаление каждого второго элемента хвост: %A" (removeEvenElemetnsTails [1;2;3;4;5;6])
    printfn "Суммируем два списка: %A" (sumLists [1;2;3] [5;6;7])
    printfn "Ищем масти: %A" (findCard [("трефи", 4); ("буби", 6); ("буби", 3); ("пики", 9); ("трефи", 1)] "трефи")
    printfn "Ищем фигуру: %A" (findCheess [("черный кололь", "5", "a"); ("белая пешка", "4", "b"); ("черный конь", "3", "b"); ("черная пешка", "5", "b")] "белая пешка" "b")
    printfn "Ищем одинаковые карты: %A" (prepareFindSameCards [[("трефи", 4);("буби", 3)];[("пики", 1);("трефи", 5)];[("трефи", 4)]])
    0
