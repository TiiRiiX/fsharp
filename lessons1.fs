let averageCubeIntInt a b =
    float(a*a*a + b*b*b)/2.0

let averageCubeFloatInt (a:float,b:int) =
    (a*a*a + float(b*b*b))/2.0

let averageCubeFloatFloat a b=
    (a*a*a + b*b*b)/2.0

let mulitNumbers a =
    let mutable x = a
    let mutable solve = 1
    while x > 0 do
        solve <- solve * (x % 10)
        x <- x / 10
    solve

let converter d =
    let y = d * 25.4
    let m = int(y) / 1000
    let cm = (int(y)-m*1000)/10
    let mm = y-float(m)*1000.0-float(cm)*10.0
    (m, cm, mm)

let interval h1 m1 s1 h2 m2 s2 =
    let mutable s = s1 - s2
    let mutable m = m1 - m2
    let mutable h = h1 - h2
    if s < 0 then
        s <- s + 60
        m <- m - 1
    if m < 0 then
        m <- m + 60
        h <- h - 1
    (h, m, s)

let taxes p0 p1 p2 p3 =
    (p1 + p0, p2 + p1 + p0, p3 + p2 + p1 + p0)

[<EntryPoint>]
let main argv =
    printfn "IntInt %A" (averageCubeIntInt 1 2)
    printfn "FloatInt %A" (averageCubeFloatInt(1.0,2))
    printfn "FloatFloat %A" (averageCubeFloatFloat 1.0 2.0)
    printfn "Произведение цифр: %A" (mulitNumbers 1234)
    match converter 21.0 with
        | (m, cm, mm) -> printfn "%Aм %Aсм %Aмм" m cm mm
    match interval 19 40 25 11 30 10 with
    //match interval 19 40 30 11 30 50 with
    //match interval 19 20 30 11 30 50 with
        | (h, m, s) -> printfn "%Aч %Aм %Aс" h m s
    match taxes 5 10 20 30 with
        | (t1, t2, t3) -> printfn "Первый: %A, Второй: %A, Третьй: %A" t1 t2 t3
    0