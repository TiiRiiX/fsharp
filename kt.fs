// Дополнительные сведения о F# см. на http://fsharp.org
// Дополнительную справку см. в проекте "Учебник по F#".

let printList a =
    for i in 0..List.length(a) - 1 do
        printf "%A " a.[i]
    printfn ""

let first x y =
    if ((x > 0) && (y > 0)) then
        1
    elif ((x < 0) && (y > 0)) then
        2
    elif ((x < 0) && (y < 0)) then
        3
    else 
        4

let second list =
    let mutable new_list = []
    for i in List.length(list) - 1..-1..0 do
        if (i % 2 = 0) then
            new_list <- list.[i]::0::new_list
        else
            new_list <- list.[i]::new_list
    new_list

[<EntryPoint>]
let main argv = 
    printfn "Четверть: %A" (first -1 -1)
    let test_list = [1..5]
    printList (second test_list)
    0 // возвращение целочисленного кода выхода
