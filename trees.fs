type BinTree =
    | Node of int*BinTree*BinTree
    | Empty


type ExprTree =
    | Op of char*ExprTree*ExprTree
    | Value of int

let myTree =
    Node (1,
        Node (0, Empty, Empty),
        Node (3,
            Node (2, Empty, Empty),
            Node (5, Empty, Empty)
        )
    )

let x = 8

let sampleTree = Op('+',
                    Op('*',
                        Value(2),
                        Value(x)
                    ),
                    Value(1)
                )

let rec insertValueInTree tree value =
    match tree with
        | Node (data, left, right) ->
            if value > data then
                Node (data, left, insertValueInTree right value)
            else
                Node (data, insertValueInTree left value, right)
        | Empty -> Node (value, Empty, Empty)

let rec lineRound tree =
    match tree with
        | Node (data, left, right) ->
            printf " %A" data
            lineRound left
            lineRound right
        | Empty -> printf ""

let rec reverceRound tree =
    match tree with
        | Node (data, left, right) ->
            reverceRound left
            reverceRound right
            printf " %A" data
        | Empty -> printf ""

let rec simRound tree =
    match tree with
        | Node (data, left, right) ->
            simRound left
            printf " %A" data
            simRound right
        | Empty -> printf ""

let rec sumTree tree = 
    match tree with
        | Node (data, left, right) ->
            data + sumTree left + sumTree right
        | Empty -> 0

let rec compute tree =
    match tree with
        | Value (x) -> x
        | Op ( op, L, R) ->
            match op with
                | '+' -> compute L + compute R
                | '-' -> compute L - compute R
                | '*' -> compute L * compute R
                | '/' -> compute L / compute R
                | _ -> 0

let rec findOp tree find =
    match tree with
        | Value (_) -> 0
        | Op (op, L, R) ->
            match op with
                | x when find = x -> 1 + (findOp L find) + (findOp R find)
                | _ -> (findOp L find) + (findOp R find)

[<EntryPoint>]
let main argv =
    printfn "%A" myTree
    printfn "%A" (insertValueInTree myTree 6)
    lineRound myTree
    printfn ""
    reverceRound myTree
    printfn ""
    simRound myTree
    printfn ""
    printfn "Сумма дерева: %A" (sumTree myTree)
    printfn "Значение дерева выражния: %A" (compute sampleTree)
    printfn "Количество операций: %A" (findOp sampleTree '*')
    0