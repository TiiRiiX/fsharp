let rec power xn x a b =
    if a >= b then
        printfn "%A" xn
    else
        printfn "%A степень: %A" xn a
        power (xn*x) x (a+1) b
        
let rec sn an n =
    if n >= 1 then
        printfn "%A %A" an n
        sn (an+(n-1)*(n-1)) (n-1)

let zerosnumber =
    let mutable count = 0
    for x in 100..700 do
        if x % 10 = 0 then
            count <- count + 1
    count

let deleteItem (n:int) a = 
    let mutable newList = []
    for i in List.length(a) - 1..-1..0 do 
        if not(i = n) then
            newList <- List.Cons(a.[i], newList)
    newList

let onlyEven a = 
    List.filter (fun x -> x % 2 = 0) a

let printList a =
    for i in 0..List.length(a) - 1 do
        printf "%A " a.[i]
    printfn ""

[<EntryPoint>]
let main argv =
    power 1 2 0 3
    //sn 1 4
    printfn "%A" zerosnumber
    let test = [0..10]
    printList (deleteItem 2 test)
    printList (onlyEven test)
    0