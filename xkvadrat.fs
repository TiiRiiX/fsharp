open System

let solve a b c =
    let D = b*b-4.*a*c in
        ((-b+sqrt(D))/(2.*a),(-b-sqrt(D))/(2.*a))

[<EntryPoint>]
let main argv =
    let a = 1.0 in 
        let b = 2.0 in
            let c = -3.0 in
                let D = b*b-4.*a*c in
                    Console.WriteLine([(-b+sqrt(D))/(2.*a);(-b-sqrt(D))/(2.*a)])
    Console.WriteLine(solve 1. 2. 3.)
    0