let energy k1 k2 k3 p = 
    if p < 500 then
        p*k1
    elif p < 1000 then
        500*k1 + (p - 500)*k2
    else
        500*k1 + 1000*k2 + (p - 1500)*k3

let path (v1:float, t1:float, v2:float, t2:float, v3:float, t3:float) =
    let half = (v1*t1 + v2*t2 + v3*t3)/2.0
    if half < v1 then
        (1,half/v1)
    elif half < v2 then
        (2,t1 + half/v2)
    else
        (3,t1 + t2 + half/v3)

let cows u v w k s t f =
    if (s*100)/(k*u) < (t*1000)/(k*v) then
        if (s*100)/(k*u) < (f*50)/(k*w) then
            ("Сено", (s*100)/(k*u))
        else
            ("Комбикорм", (f*50)/(k*w))
    else
        ("Силос", (t*1000)/(k*v))

[<EntryPoint>]
let main argv =
    printfn "Энергия: %A" (energy 5 10 15 700)
    printfn "Путь: %A" (path(5.0, 5.0, 10.0, 10.0, 20.0, 20.0))
    match path(5.0, 5.0, 10.0, 10.0, 20.0, 20.0) with
        | (c, time) -> printfn "Участок: %A, Время: %A" c time
    match cows 10 10 10 20 8 8 8 with
        | (s, d) -> printfn "Быстрее кончится: %A за %A дней" s d
    0